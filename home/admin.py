from django.contrib import admin

from home.models import Procurador


# Register your models here.
class ProcuradorAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'apellidos')

admin.site.register(Procurador, ProcuradorAdmin)
