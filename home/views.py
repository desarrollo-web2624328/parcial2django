import generics
from django.shortcuts import render

from home.models import Procurador


# Create your views here.
class ProcuradorVista(generics.ListCreateAPIView):
    queryset=Procurador.objects.all()